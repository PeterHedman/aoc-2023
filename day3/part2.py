def sum_gear_ratios(schematic):
    def is_number(char):
        return char.isdigit()

    def adjacent_numbers(i, j):
        nums = set()
        directions = [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]

        for dx, dy in directions:
            x, y = i + dx, j + dy
            num = ''
            while 0 <= x < len(schematic) and 0 <= y < len(schematic[0]) and is_number(schematic[x][y]):
                num = schematic[x][y] + num
                x -= dx
                y -= dy
            if num:
                nums.add(int(num))

        return nums

    total_ratio_sum = 0

    for i, row in enumerate(schematic):
        for j, char in enumerate(row):
            if char == '*':
                numbers = adjacent_numbers(i, j)
                if len(numbers) == 2:
                    total_ratio_sum += prod(numbers)

    return total_ratio_sum

from math import prod

# Test case
test_schematic = [
    "467..114..",
    "...*......",
    "..35..633.",
    "......#...",
    "617*......",
    ".....+.58.",
    "..592.....",
    "......755.",
    "...$.*....",
    ".664.598.."
]

print("Test Case Result:", sum_gear_ratios(test_schematic))

# Actual data
file_path = 'input.txt'
schematic = read_schematic_from_file(file_path)
print("Actual Data Result:", sum_gear_ratios(schematic))

