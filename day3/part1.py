def sum_part_numbers(schematic):
    def is_symbol(char):
        return char in '*#$+%/&@-='

    def is_adjacent_to_symbol(x, y):
        directions = [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]
        for dx, dy in directions:
            nx, ny = x + dx, y + dy
            if 0 <= nx < len(schematic) and 0 <= ny < len(schematic[0]) and is_symbol(schematic[nx][ny]):
                return True
        return False

    total_sum = 0
    for i, row in enumerate(schematic):
        j = 0
        while j < len(row):
            if row[j].isdigit():
                num = row[j]
                j += 1
                while j < len(row) and row[j].isdigit():
                    num += row[j]
                    j += 1
                # Check adjacent symbols for each digit in the number
                for k in range(j - len(num), j):
                    if is_adjacent_to_symbol(i, k):
                        total_sum += int(num)
                        break
            else:
                j += 1
    
    return total_sum

def read_schematic_from_file(file_path):
    with open(file_path, 'r') as file:
        return [line.strip() for line in file.readlines()]

file_path = 'input.txt'
schematic = read_schematic_from_file(file_path)
print(sum_part_numbers(schematic))

