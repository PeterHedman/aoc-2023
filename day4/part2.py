import argparse
from collections import defaultdict

def calculate_total_cards(cards):
    total_cards = 0
    card_data = []

    # Preprocessing to store winning numbers and your numbers for each card
    for card in cards:
        _, numbers = card.split(': ')
        winning, yours = numbers.split('|')
        winning_numbers = set(map(int, winning.split()))
        your_numbers = list(map(int, yours.split()))
        card_data.append((winning_numbers, your_numbers))

    # To keep track of total cards processed
    total_scratchcards = defaultdict(int)

    # Recursive function to process each card and count how many scratchcards we win
    def process_card(card_index):
        if card_index >= len(card_data):
            return 0
        
        # If already processed this many times, skip it to avoid unnecessary recomputation
        if total_scratchcards[card_index] > 0:
            return total_scratchcards[card_index]

        winning_numbers, your_numbers = card_data[card_index]
        
        # Calculate the number of matches
        matches = sum(1 for number in your_numbers if number in winning_numbers)
        
        # Initial card counts itself
        total_won_cards = 1
        
        # If there are matches, recursively process next cards
        if matches > 0:
            for i in range(1, matches + 1):
                if card_index + i < len(card_data):
                    total_won_cards += process_card(card_index + i)

        # Store the result to avoid recomputing
        total_scratchcards[card_index] = total_won_cards
        return total_won_cards

    # Process all cards starting from each original card
    for i in range(len(card_data)):
        total_cards += process_card(i)

    return total_cards

def main():
    # Set up argument parser
    parser = argparse.ArgumentParser(description="Calculate total scratchcards from winning scratchcards.")
    parser.add_argument('input_file', type=str, help='The input file containing scratchcard data')
    
    # Parse arguments
    args = parser.parse_args()

    # Read input file
    with open(args.input_file, 'r') as file:
        cards = [line.strip() for line in file if line.strip()]

    # Calculate and print total scratchcards
    total_cards = calculate_total_cards(cards)
    print(total_cards)

if __name__ == "__main__":
    main()

