import argparse

def calculate_total_points(cards):
    total_points = 0

    for card in cards:
        # Remove the 'Card X:' part and split the card into winning numbers and your numbers
        _, numbers = card.split(': ')
        winning, yours = numbers.split('|')
        
        # Convert the numbers to sets for easy lookup
        winning_numbers = set(map(int, winning.split()))
        your_numbers = list(map(int, yours.split()))
        
        # Calculate the number of matches
        matches = 0
        for number in your_numbers:
            if number in winning_numbers:
                matches += 1
        
        # Calculate the points for this card
        if matches > 0:
            points = 1
            for _ in range(1, matches):
                points *= 2
            total_points += points
    
    return total_points

def main():
    # Set up argument parser
    parser = argparse.ArgumentParser(description="Calculate total points from scratchcards.")
    parser.add_argument('input_file', type=str, help='The input file containing scratchcard data')
    
    # Parse arguments
    args = parser.parse_args()

    # Read input file
    with open(args.input_file, 'r') as file:
        cards = [line.strip() for line in file if line.strip()]

    # Calculate and print total points
    total_points = calculate_total_points(cards)
    print(total_points)

if __name__ == "__main__":
    main()

