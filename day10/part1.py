import sys

def read_input(file_path):
    with open(file_path, 'r') as file:
        return file.read().strip()

def parse_input(sketch):
    grid = [list(line) for line in sketch.split('\n') if line.strip()]
    return grid

def get_neighbors(x, y, grid):
    neighbors = []
    if grid[x][y] in ['|', 'L', 'J', 'S']:  # Connects north
        if x > 0 and grid[x-1][y] in ['|', '7', 'F', 'S']:
            neighbors.append((x-1, y))
    if grid[x][y] in ['|', '7', 'F', 'S']:  # Connects south
        if x < len(grid) - 1 and grid[x+1][y] in ['|', 'L', 'J', 'S']:
            neighbors.append((x+1, y))
    if grid[x][y] in ['-', 'L', 'F', 'S']:  # Connects west
        if y > 0 and grid[x][y-1] in ['-', 'J', '7', 'S']:
            neighbors.append((x, y-1))
    if grid[x][y] in ['-', 'J', '7', 'S']:  # Connects east
        if y < len(grid[0]) - 1 and grid[x][y+1] in ['-', 'L', 'F', 'S']:
            neighbors.append((x, y+1))
    return neighbors

def find_loop(grid):
    # Debug: Print the grid
    for row in grid:
        print(''.join(row))

    # Find starting position
    try:
        start_x, start_y = next((x, y) for x in range(len(grid)) for y in range(len(grid[0])) if grid[x][y] == 'S')
    except StopIteration:
        raise ValueError("No starting position 'S' found in the grid.")

    # DFS to find loop and distances
    visited, stack = set(), [(start_x, start_y, 0)]
    distances = {}
    while stack:
        x, y, dist = stack.pop()
        if (x, y) not in visited:
            visited.add((x, y))
            distances[(x, y)] = dist
            for neighbor in get_neighbors(x, y, grid):
                if neighbor not in visited:
                    stack.append((neighbor[0], neighbor[1], dist + 1))

    # Find farthest point
    farthest_distance = max(distances.values())
    return farthest_distance

def main(file_path):
    sketch = read_input(file_path)
    grid = parse_input(sketch)
    farthest_distance = find_loop(grid)
    print(f"The farthest point from the start is {farthest_distance} steps away.")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py <input_file>")
        sys.exit(1)

    input_file = sys.argv[1]
    main(input_file)
