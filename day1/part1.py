import argparse
import re

def convert_word_to_digit(word):
    """Convert a spelled-out word of a digit to its numeric equivalent."""
    word_to_digit = {
        'one': '1', 'two': '2', 'three': '3', 'four': '4', 'five': '5',
        'six': '6', 'seven': '7', 'eight': '8', 'nine': '9'
    }
    return word_to_digit.get(word.lower(), None)

def find_first_last_digit(line):
    # Regular expression to find either digits or spelled-out numbers
    digit_pattern = re.compile(r'\d|one|two|three|four|five|six|seven|eight|nine', re.IGNORECASE)
    
    matches = digit_pattern.findall(line)

    if not matches or len(matches) < 2:
        return 0  # No valid first and last digit found, should not happen with valid input
    
    # Find first and last "digit" (either numeric or word)
    first_digit = matches[0]
    last_digit = matches[-1]

    # Convert them to actual digits if they are spelled-out words
    if not first_digit.isdigit():
        first_digit = convert_word_to_digit(first_digit)
    if not last_digit.isdigit():
        last_digit = convert_word_to_digit(last_digit)
    
    return int(first_digit + last_digit)

def calculate_calibration_sum(lines):
    total_sum = 0
    for line in lines:
        calibration_value = find_first_last_digit(line)
        total_sum += calibration_value
    return total_sum

def main():
    # Set up argument parser
    parser = argparse.ArgumentParser(description="Calculate the sum of calibration values from an input file.")
    parser.add_argument('input_file', type=str, help='The input file containing calibration lines.')
    
    # Parse arguments
    args = parser.parse_args()

    # Read input file
    with open(args.input_file, 'r') as file:
        lines = [line.strip() for line in file if line.strip()]

    # Calculate and print the sum of calibration values
    result = calculate_calibration_sum(lines)
    print(result)

if __name__ == "__main__":
    main()

