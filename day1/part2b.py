import re

def sum_calibration_values(file_path):
    digit_pattern = r'(one|two|three|four|five|six|seven|eight|nine|\d)'

    total_sum = 0

    def find_first_last_digits(line):
        matches = re.findall(digit_pattern, line)
        # Convert spelled-out numbers to digits
        for i, match in enumerate(matches):
            if match.isalpha():
                matches[i] = str(['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'].index(match) + 1)
        return matches[0], matches[-1] if matches else ('0', '0')

    with open(file_path, 'r') as file:
        for line in file:
            line = line.strip()
            print(f"Processing line: '{line}'")

            first_digit, last_digit = find_first_last_digits(line)

            # Check if there's only one number in the line
            if first_digit == last_digit and len(re.findall(digit_pattern, line)) == 1:
                combined_value = int(first_digit) * 10
                #combined_value = 0
            else:
                combined_value = int(first_digit) * 10 + int(last_digit)

            total_sum += combined_value
            print(f"Combined value for line: {combined_value}, Running total: {total_sum}")

    return total_sum

file_path = 'input.txt'
#file_path = 'example2.txt'
print("Total sum of calibration values:", sum_calibration_values(file_path))

