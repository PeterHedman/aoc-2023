import re

def sum_calibration_values(file_path):
    # Updated regex pattern to match spelled-out numbers and digits
    digit_pattern = r'(?:(one|two|three|four|five|six|seven|eight|nine)|(\d))'
    total_sum = 0

    def convert_spelled_number(word):
        spelled_numbers = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
        return spelled_numbers.index(word) + 1 if word in spelled_numbers else word

    def find_first_last_digits(line):
        matches = re.findall(digit_pattern, line)
        # Flatten the list of tuples and filter out empty strings
        matches = [num for tup in matches for num in tup if num]
        # Convert spelled numbers to digits
        matches = [convert_spelled_number(num) for num in matches]
        return matches[0], matches[-1] if matches else ('0', '0'), len(matches)

    with open(file_path, 'r') as file:
        for line in file:
            line = line.strip()

            first_digit, last_digit, match_count = find_first_last_digits(line)

            # Handle single digit lines
            if match_count == 1:
                combined_value = int(first_digit) * 11
            else:
                combined_value = int(first_digit) * 10 + int(last_digit)

            total_sum += combined_value

    return total_sum

file_path = 'input.txt'
print("Total sum of calibration values:", sum_calibration_values(file_path))

