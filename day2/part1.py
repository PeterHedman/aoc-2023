def is_game_possible(cube_sets, constraints):
    max_cubes = {'red': 0, 'green': 0, 'blue': 0}
    for set in cube_sets:
        for color, count in set.items():
            max_cubes[color] = max(max_cubes[color], count)
    return all(max_cubes[color] <= constraints[color] for color in max_cubes)

def main():
    with open('input.txt', 'r') as file:
        games = file.readlines()

    constraints = {'red': 12, 'green': 13, 'blue': 14}
    sum_of_ids = 0

    for game in games:
        parts = game.strip().split(': ')
        game_id = int(parts[0].split(' ')[1])
        cube_sets = [part.split(', ') for part in parts[1].split('; ')]
        cube_sets_processed = []

        for set in cube_sets:
            cube_dict = {}
            for cube in set:
                count, color = cube.split(' ')
                cube_dict[color] = int(count)
            cube_sets_processed.append(cube_dict)

        if is_game_possible(cube_sets_processed, constraints):
            sum_of_ids += game_id

    print("Sum of IDs for possible games:", sum_of_ids)

main()

