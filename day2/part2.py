def minimum_cubes_required(cube_sets):
    min_cubes = {'red': 0, 'green': 0, 'blue': 0}
    for set in cube_sets:
        for color, count in set.items():
            min_cubes[color] = max(min_cubes[color], count)
    return min_cubes

def calculate_power(cube_set):
    return cube_set['red'] * cube_set['green'] * cube_set['blue']

def main():
    with open('input.txt', 'r') as file:
        games = file.readlines()

    total_power = 0

    for game in games:
        parts = game.strip().split(': ')
        cube_sets = [part.split(', ') for part in parts[1].split('; ')]
        cube_sets_processed = []

        for set in cube_sets:
            cube_dict = {}
            for cube in set:
                count, color = cube.split(' ')
                cube_dict[color] = int(count)
            cube_sets_processed.append(cube_dict)

        min_cubes = minimum_cubes_required(cube_sets_processed)
        total_power += calculate_power(min_cubes)

    print("Sum of the power of minimum sets:", total_power)

main()

