package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"math/big"
	"os"
	"regexp"
	"strconv"
)

// Function to count the number of valid H for a given race
func countH(T, D int) int {
	// Handle edge cases where no valid H exists
	if T <= 0 {
		return 0
	}
	if D < 0 {
		if T >= 2 {
			return T - 1 // H can be from 1 to T-1
		}
		return 0
	}

	// Calculate the discriminant of the quadratic equation H^2 - T*H + D = 0
	discriminant := float64(T*T) - 4.0*float64(D)
	if discriminant <= 0 {
		return 0 // No real roots, hence no valid H
	}

	// Calculate the square root of the discriminant
	sqrtDiscriminant := math.Sqrt(discriminant)

	// Find the roots of the quadratic equation
	H_lower := (float64(T) - sqrtDiscriminant) / 2.0
	H_upper := (float64(T) + sqrtDiscriminant) / 2.0

	// Calculate the ceiling of H_lower and floor of H_upper to find integer H
	// Adding a small epsilon (1e-9) to handle floating-point precision issues
	low := int(math.Ceil(H_lower + 1e-9))
	high := int(math.Floor(H_upper - 1e-9))

	// Ensure H is within the valid range [1, T-1]
	if low < 1 {
		low = 1
	}
	if high > T-1 {
		high = T - 1
	}
	if low > high {
		return 0
	}

	return high - low + 1
}

func main() {
	// Check if the user has provided exactly one command-line argument (the file path)
	if len(os.Args) != 2 {
		log.Fatalf("Usage: %s <input_file_path>\n", os.Args[0])
	}

	inputFilePath := os.Args[1]

	// Open the input file
	file, err := os.Open(inputFilePath)
	if err != nil {
		log.Fatalf("Error opening file '%s': %v\n", inputFilePath, err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var Ts, Ds []int
	lineNumber := 0

	// Regular expression to match integers (positive and negative)
	re := regexp.MustCompile(`-?\d+`)

	// Read the file line by line
	for scanner.Scan() {
		line := scanner.Text()
		lineNumber++
		// Extract all integer tokens from the line
		tokens := re.FindAllString(line, -1)
		if lineNumber == 1 {
			// First line: Time Allowed (T) values
			for _, tok := range tokens {
				t, err := strconv.Atoi(tok)
				if err != nil {
					log.Fatalf("Error parsing T on line 1: '%s' is not a valid integer.\n", tok)
				}
				Ts = append(Ts, t)
			}
		} else if lineNumber == 2 {
			// Second line: Record Distance (D) values
			for _, tok := range tokens {
				d, err := strconv.Atoi(tok)
				if err != nil {
					log.Fatalf("Error parsing D on line 2: '%s' is not a valid integer.\n", tok)
				}
				Ds = append(Ds, d)
			}
		} else {
			// Ignore any additional lines beyond the first two
			break
		}
	}

	// Check for scanning errors
	if err := scanner.Err(); err != nil {
		log.Fatalf("Error reading file '%s': %v\n", inputFilePath, err)
	}

	// Validate that exactly two lines were read
	if lineNumber < 2 {
		log.Fatalf("Input file '%s' does not contain enough lines. Expected 2 lines (T and D values).\n", inputFilePath)
	}

	// Validate that the number of T's and D's are equal
	if len(Ts) != len(Ds) {
		log.Fatalf("Mismatch in number of T and D values. T count: %d, D count: %d\n", len(Ts), len(Ds))
	}

	// Initialize the product as a big integer to handle large results
	product := big.NewInt(1)

	// Iterate through each race and calculate the number of valid H
	for i := 0; i < len(Ts); i++ {
		T := Ts[i]
		D := Ds[i]
		count := countH(T, D)
		// Multiply the current count with the running product
		product.Mul(product, big.NewInt(int64(count)))
	}

	// Output the final product
	fmt.Println(product)
}
