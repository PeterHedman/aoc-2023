def calculate_lava_capacity_from_file(file_path):
    with open(file_path, 'r') as file:
        dig_plan = [line.strip().split(' ')[0] + ' ' + line.strip().split(' ')[1] for line in file]

    return calculate_lava_capacity(dig_plan)

def parse_instruction(instruction):
    direction, distance = instruction.split()
    return direction, int(distance)


def calculate_lava_capacity(dig_plan):
    path = set()
    x, y = 0, 0  # Starting point
    path.add((x, y))

    # Track the path of the digger
    for instruction in dig_plan:
        direction, distance = parse_instruction(instruction)
        for _ in range(distance):
            if direction == 'U':
                y -= 1
            elif direction == 'D':
                y += 1
            elif direction == 'L':
                x -= 1
            elif direction == 'R':
                x += 1
            path.add((x, y))

    # Determine the bounds of the trench
    min_x = min(x for x, y in path)
    max_x = max(x for x, y in path)
    min_y = min(y for x, y in path)
    max_y = max(y for x, y in path)

    # Scan for interior cells
    interior_area = 0
    for i in range(min_x, max_x + 1):
        inside = False
        for j in range(min_y, max_y + 1):
            if (i, j) in path:
                inside = not inside
            elif inside and (i, j) not in path:
                interior_area += 1

    # Total capacity = Trench area + Interior area - Initial dug out space
    total_capacity = len(path) + interior_area - 1
    return total_capacity
# Example   case
dig_plan = [
    "R 6", "D 5", "L 2", "D 2", "R 2", "D 2", "L 5", "U 2",
    "L 1", "U 2", "R 2", "U 3", "L 2", "U 2"
]
print(calculate_lava_capacity(dig_plan))

# Real use csase
file_path = 'input.txt'
print(calculate_lava_capacity_from_file(file_path))
