def parse_workflow(line):
    name, rules_str = line.split('{')
    rules = rules_str[:-1].split(',')
    parsed_rules = []
    for rule in rules:
        if rule in ['A', 'R']:
            parsed_rules.append((rule, None, rule))
        else:
            condition, result = rule.split(':')
            parsed_rules.append((condition[0], condition[1:], result))
    return name, parsed_rules

def parse_part(line):
    ratings = {x.split('=')[0]: int(x.split('=')[1]) for x in line[1:-1].split(',')}
    return ratings

def process_part(part, workflows):
    current_workflow = 'in'
    while True:
        for condition, value, result in workflows[current_workflow]:
            if result in ['A', 'R']:
                return result, part
            if condition == 'x' and part['x'] > int(value):
                current_workflow = result
                break
            elif condition == 'm' and part['m'] < int(value):
                current_workflow = result
                break
            elif condition == 'a' and part['a'] > int(value):
                current_workflow = result
                break
            elif condition == 's' and part['s'] < int(value):
                current_workflow = result
                break

workflows = {}
parts = []

# Replace with your input parsing
with open('test-input.txt', 'r') as file:
    lines = file.readlines()
    for line in lines:
        if '{' in line:
            name, rules = parse_workflow(line)
            workflows[name] = rules
        elif line.strip():
            parts.append(parse_part(line.strip()))

total = 0
for part in parts:
    result, ratings = process_part(part, workflows)
    if result == 'A':
        total += sum(ratings.values())

print("Total sum of ratings for accepted parts:", total)

