import argparse
import sys
from collections import deque

def parse_arguments():
    parser = argparse.ArgumentParser(description='Find the longest hike on Snow Island (Part Two).')
    parser.add_argument('-f', '--file', type=str, required=True, help='Input file containing the map.')
    return parser.parse_args()

def read_map(file_path):
    try:
        with open(file_path, 'r') as f:
            grid = [line.rstrip('\n') for line in f]
        return grid
    except FileNotFoundError:
        print(f"Error: File '{file_path}' not found.")
        sys.exit(1)
    except Exception as e:
        print(f"Error reading file '{file_path}': {e}")
        sys.exit(1)

def find_start_end(grid):
    start = None
    end = None
    # Find start in the first row
    for y, cell in enumerate(grid[0]):
        if cell == '.':
            start = (0, y)
            break
    # Find end in the last row
    last_row = len(grid) - 1
    for y, cell in enumerate(grid[last_row]):
        if cell == '.':
            end = (last_row, y)
            break
    return start, end

def is_passable(cell):
    # In Part Two, all slope tiles are treated as normal paths
    return cell != '#'

def preprocess_grid(grid):
    """
    Assign a unique index to each passable tile and create adjacency lists.
    """
    tile_indices = {}
    index = 0
    rows = len(grid)
    cols = len(grid[0]) if rows > 0 else 0
    for x in range(rows):
        for y in range(cols):
            if is_passable(grid[x][y]):
                tile_indices[(x, y)] = index
                index += 1
    total_tiles = index
    # Create adjacency list
    adjacency = [[] for _ in range(total_tiles)]
    directions = [(-1,0), (1,0), (0,-1), (0,1)]
    for (x, y), idx in tile_indices.items():
        for dx, dy in directions:
            nx, ny = x + dx, y + dy
            if (nx, ny) in tile_indices:
                adjacency[idx].append(tile_indices[(nx, ny)])
    return tile_indices, adjacency, total_tiles

def popcount(x):
    """
    Counts the number of set bits (1s) in the integer x.
    Efficient implementation using Brian Kernighan's Algorithm.
    """
    count = 0
    while x:
        x &= x - 1
        count += 1
    return count

def longest_hike_part_two(grid, start, end):
    tile_indices, adjacency, total_tiles = preprocess_grid(grid)
    start_idx = tile_indices[start]
    end_idx = tile_indices[end]

    max_steps = 0
    stack = []
    # Initialize visited as bitmask
    visited_initial = 1 << start_idx
    stack.append( (start_idx, visited_initial, 0) )  # (current_tile, visited_bitmask, steps)

    while stack:
        current_tile, visited, steps = stack.pop()

        # If reached end, update max_steps
        if current_tile == end_idx:
            if steps > max_steps:
                max_steps = steps
            continue

        # Early termination: if remaining tiles can't surpass current max
        remaining_tiles = total_tiles - popcount(visited)
        if steps + remaining_tiles <= max_steps:
            continue

        # Explore neighbors
        neighbors = adjacency[current_tile]
        # Optionally, sort neighbors based on proximity to end to explore promising paths first
        # This can lead to earlier discoveries of longer paths
        # However, sorting can add overhead, so it's optional
        # Uncomment the following lines to enable neighbor sorting
        # neighbors_sorted = sorted(neighbors, key=lambda n: abs(n - end_idx))
        # for neighbor in neighbors_sorted:
        for neighbor in neighbors:
            if not (visited & (1 << neighbor)):
                # Set the neighbor as visited
                new_visited = visited | (1 << neighbor)
                new_steps = steps + 1
                stack.append( (neighbor, new_visited, new_steps) )

    return max_steps

def main():
    args = parse_arguments()
    grid = read_map(args.file)
    start, end = find_start_end(grid)
    if not start:
        print("Error: Start position not found in the first row.")
        sys.exit(1)
    if not end:
        print("Error: End position not found in the last row.")
        sys.exit(1)
    max_steps = longest_hike_part_two(grid, start, end)
    print(f"The longest hike contains {max_steps} steps.")

if __name__ == "__main__":
    main()

