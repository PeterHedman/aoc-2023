import argparse
from collections import deque

def parse_arguments():
    parser = argparse.ArgumentParser(description='Find the longest hike on Snow Island.')
    parser.add_argument('-f', '--file', type=str, required=True, help='Input file containing the map.')
    return parser.parse_args()

def read_map(file_path):
    with open(file_path, 'r') as f:
        grid = [line.rstrip('\n') for line in f]
    return grid

def find_start_end(grid):
    start = None
    end = None
    # Find start in the first row
    for y, cell in enumerate(grid[0]):
        if cell == '.':
            start = (0, y)
            break
    # Find end in the last row
    last_row = len(grid) - 1
    for y, cell in enumerate(grid[last_row]):
        if cell == '.':
            end = (last_row, y)
            break
    return start, end

def get_direction(symbol):
    return {
        '^': (-1, 0),
        'v': (1, 0),
        '<': (0, -1),
        '>': (0, 1)
    }.get(symbol, None)

def longest_hike(grid, start, end):
    rows = len(grid)
    cols = len(grid[0]) if rows > 0 else 0

    # Directions: up, down, left, right
    directions = [(-1,0), (1,0), (0,-1), (0,1)]

    max_steps = 0

    # Stack for DFS: each element is (x, y, visited_set, steps, constraint)
    stack = deque()
    initial_visited = set()
    initial_visited.add(start)
    # Initialize steps to 0 instead of 1
    stack.append( (start[0], start[1], initial_visited, 0, None) )

    while stack:
        x, y, visited, steps, constraint = stack.pop()

        # If reached end, update max_steps
        if (x, y) == end:
            if steps > max_steps:
                max_steps = steps
            continue

        # Determine possible moves
        possible_moves = []

        if constraint:
            # Must move in the constrained direction
            dx, dy = constraint
            nx, ny = x + dx, y + dy
            if 0 <= nx < rows and 0 <= ny < cols:
                cell = grid[nx][ny]
                if cell != '#' and (nx, ny) not in visited:
                    possible_moves.append( (nx, ny) )
        else:
            # Can move in any direction
            for dx, dy in directions:
                nx, ny = x + dx, y + dy
                if 0 <= nx < rows and 0 <= ny < cols:
                    cell = grid[nx][ny]
                    if cell != '#' and (nx, ny) not in visited:
                        possible_moves.append( (nx, ny) )

        for move in possible_moves:
            nx, ny = move
            cell = grid[nx][ny]
            # Determine new constraint
            new_constraint = get_direction(cell) if cell in '^v<>' else None
            # Create a new visited set
            new_visited = visited.copy()
            new_visited.add( (nx, ny) )
            # Push new state to stack
            stack.append( (nx, ny, new_visited, steps + 1, new_constraint) )

    return max_steps

def main():
    args = parse_arguments()
    grid = read_map(args.file)
    start, end = find_start_end(grid)
    if not start or not end:
        print("Start or end position not found.")
        return
    max_steps = longest_hike(grid, start, end)
    print(f"The longest hike contains {max_steps} steps.")

if __name__ == "__main__":
    main()

