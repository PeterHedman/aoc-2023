#include <bits/stdc++.h>
using namespace std;

// Type definitions
typedef long long ll;
typedef pair<int, int> pii;
typedef unsigned long long ull;

// Function to read the map from a file
vector<string> read_map(const string& file_path) {
    vector<string> grid;
    ifstream infile(file_path);
    if (!infile) {
        cerr << "Error: File '" << file_path << "' not found." << endl;
        exit(1);
    }
    string line;
    while (getline(infile, line)) {
        grid.push_back(line);
    }
    infile.close();
    return grid;
}

// Function to find start and end positions
pair<pii, pii> find_start_end(const vector<string>& grid) {
    pii start = {-1, -1};
    pii end = {-1, -1};
    // Find start in the first row
    for (int y = 0; y < grid[0].size(); ++y) {
        if (grid[0][y] == '.') {
            start = {0, y};
            break;
        }
    }
    // Find end in the last row
    int last_row = grid.size() - 1;
    for (int y = 0; y < grid[last_row].size(); ++y) {
        if (grid[last_row][y] == '.') {
            end = {last_row, y};
            break;
        }
    }
    return {start, end};
}

// Function to check if a cell is passable
bool is_passable(char cell) {
    return cell != '#';
}

// Function to preprocess the grid: assign indices and create adjacency lists
tuple<vector<pii>, vector<vector<int>>, int> preprocess_grid(const vector<string>& grid) {
    // Assign unique indices to passable tiles
    map<pii, int> tile_indices;
    int index = 0;
    int rows = grid.size();
    int cols = grid[0].size();
    for (int x = 0; x < rows; ++x) {
        for (int y = 0; y < cols; ++y) {
            if (is_passable(grid[x][y])) {
                tile_indices[{x, y}] = index++;
            }
        }
    }
    int total_tiles = index;
    // Debug statement
    cout << "Total passable tiles: " << total_tiles << endl;

    // Create adjacency lists
    vector<vector<int>> adjacency(total_tiles, vector<int>());
    // Directions: up, down, left, right
    vector<pair<int, int>> directions = { {-1,0}, {1,0}, {0,-1}, {0,1} };
    for (auto &[pos, idx] : tile_indices) {
        int x = pos.first;
        int y = pos.second;
        for (auto &[dx, dy] : directions) {
            int nx = x + dx;
            int ny = y + dy;
            if (nx >=0 && nx < rows && ny >=0 && ny < grid[nx].size()) {
                if (is_passable(grid[nx][ny])) {
                    pii neighbor_pos = {nx, ny};
                    if (tile_indices.find(neighbor_pos) != tile_indices.end()) {
                        int neighbor_idx = tile_indices[neighbor_pos];
                        adjacency[idx].push_back(neighbor_idx);
                    }
                }
            }
        }
    }
    // Create a list of positions for distance calculations
    vector<pii> positions(total_tiles);
    for (auto &[pos, idx] : tile_indices) {
        positions[idx] = pos;
    }
    return {positions, adjacency, total_tiles};
}

// Optional: Function to compute Manhattan distance
int manhattan_distance(const pii& a, const pii& b) {
    return abs(a.first - b.first) + abs(a.second - b.second);
}

// Function to perform BFS to check if a path exists
bool bfs(int start_idx, int end_idx, const vector<vector<int>>& adjacency, int total_tiles) {
    vector<bool> visited(total_tiles, false);
    queue<int> q;
    q.push(start_idx);
    visited[start_idx] = true;
    while (!q.empty()) {
        int current = q.front(); q.pop();
        if (current == end_idx) return true;
        for (auto &neighbor : adjacency[current]) {
            if (!visited[neighbor]) {
                visited[neighbor] = true;
                q.push(neighbor);
            }
        }
    }
    return false;
}

// Iterative DFS with bit packing using std::vector<uint64_t>
ll iterative_dfs_bit_packing(int start_idx, int end_idx, const vector<vector<int>>& adjacency, int total_tiles, const vector<pii>& positions, const pii& end_pos) {
    ll max_steps = 0;
    // Calculate number of 64-bit integers needed
    int num_uint64 = (total_tiles + 63) / 64;
    // Stack elements: (current_tile, visited_bitmask, steps)
    // Using vector<uint64_t> for visited_bitmask
    stack<tuple<int, vector<ull>, int>> stk;
    vector<ull> visited_initial(num_uint64, 0);
    visited_initial[start_idx / 64] |= (1ULL << (start_idx % 64));
    stk.emplace(start_idx, visited_initial, 0);

    while (!stk.empty()) {
        auto [current, visited, steps] = stk.top();
        stk.pop();

        // If reached end, update max_steps
        if (current == end_idx) {
            if (steps > max_steps) {
                max_steps = steps;
                // Uncomment the next line for debugging purposes
                // cout << "Reached end with steps: " << steps << endl;
            }
            continue;
        }

        // Early termination: remaining tiles cannot surpass current max
        int remaining_tiles = total_tiles - 0; // Placeholder, implement accurate count if needed
        if (steps + remaining_tiles <= max_steps) {
            continue;
        }

        // Explore neighbors sorted by proximity to end (closer first)
        vector<int> neighbors_sorted = adjacency[current];
        sort(neighbors_sorted.begin(), neighbors_sorted.end(), [&](const int &a, const int &b) -> bool {
            int dist_a = manhattan_distance(positions[a], end_pos);
            int dist_b = manhattan_distance(positions[b], end_pos);
            return dist_a < dist_b; // Closer first
        });

        for (auto &neighbor : neighbors_sorted) {
            int word = neighbor / 64;
            int bit = neighbor % 64;
            if ((visited[word] & (1ULL << bit)) == 0) {
                // Create a new visited bitmask for the next state
                vector<ull> new_visited = visited;
                new_visited[word] |= (1ULL << bit);
                // Push the new state onto the stack
                stk.emplace(neighbor, new_visited, steps + 1);
            }
        }
    }

    return max_steps;
}

int main(int argc, char* argv[]) {
    // Parse command-line arguments
    if (argc != 3) {
        cerr << "Usage: " << argv[0] << " -f <file_path>" << endl;
        return 1;
    }
    string flag = argv[1];
    if (flag != "-f" && flag != "--file") {
        cerr << "Invalid flag. Use -f or --file to specify the input file." << endl;
        return 1;
    }
    string file_path = argv[2];

    // Read the map
    vector<string> grid = read_map(file_path);

    // Find start and end positions
    pair<pii, pii> positions = find_start_end(grid);
    pii start = positions.first;
    pii end = positions.second;
    if (start.first == -1 || start.second == -1) {
        cerr << "Error: Start position not found in the first row." << endl;
        return 1;
    }
    if (end.first == -1 || end.second == -1) {
        cerr << "Error: End position not found in the last row." << endl;
        return 1;
    }

    // Preprocess the grid
    auto [positions_list, adjacency, total_tiles] = preprocess_grid(grid);

    // Get start and end indices
    // Create a mapping from positions to indices
    map<pii, int> pos_to_idx;
    for (int idx = 0; idx < positions_list.size(); ++idx) {
        pos_to_idx[positions_list[idx]] = idx;
    }
    int start_idx = pos_to_idx[start];
    int end_idx = pos_to_idx[end];

    // Debug statements
    cout << "Start position: (" << start.first << ", " << start.second << "), Index: " << start_idx << endl;
    cout << "End position: (" << end.first << ", " << end.second << "), Index: " << end_idx << endl;

    // Uncomment these lines for debugging adjacency lists
    /*
    cout << "Adjacency list for Start Index (" << start_idx << "): ";
    for (auto &neighbor : adjacency[start_idx]) {
        cout << neighbor << " ";
    }
    cout << endl;

    cout << "Adjacency list for End Index (" << end_idx << "): ";
    for (auto &neighbor : adjacency[end_idx]) {
        cout << neighbor << " ";
    }
    cout << endl;
    */

    // Perform BFS to check if a path exists
    bool path_exists = bfs(start_idx, end_idx, adjacency, total_tiles);
    if (!path_exists) {
        cout << "No path exists between start and end tiles." << endl;
        return 0;
    } else {
        cout << "A path exists between start and end tiles." << endl;
    }

    // Initialize maximum steps
    ll max_steps = 0;

    // Perform Iterative DFS with vector<bool>
    // Uncomment the following lines if you want to use the vector<bool> implementation
    /*
    max_steps = iterative_dfs(start_idx, end_idx, adjacency, total_tiles, positions_list, end);
    */

    // Alternatively, use the bit packing implementation (requires further optimization)
    // For now, we'll proceed with vector<bool>
    max_steps = 0;
    // Stack elements: (current_tile, visited_bitmask, steps)
    stack<tuple<int, vector<bool>, int>> stk;
    vector<bool> visited_initial(total_tiles, false);
    visited_initial[start_idx] = true;
    stk.emplace(start_idx, visited_initial, 0);

    while (!stk.empty()) {
        auto [current, visited, steps] = stk.top();
        stk.pop();

        // If reached end, update max_steps
        if (current == end_idx) {
            if (steps > max_steps) {
                max_steps = steps;
                // Uncomment the next line for debugging purposes
                // cout << "Reached end with steps: " << steps << endl;
            }
            continue;
        }

        // Early termination: remaining tiles cannot surpass current max
        int remaining_tiles = total_tiles - count(visited.begin(), visited.end(), true);
        if (steps + remaining_tiles <= max_steps) {
            continue;
        }

        // Explore neighbors sorted by proximity to end (closer first)
        vector<int> neighbors_sorted = adjacency[current];
        sort(neighbors_sorted.begin(), neighbors_sorted.end(), [&](const int &a, const int &b) -> bool {
            int dist_a = manhattan_distance(positions_list[a], end);
            int dist_b = manhattan_distance(positions_list[b], end);
            return dist_a < dist_b; // Closer first
        });

        for (auto &neighbor : neighbors_sorted) {
            if (!visited[neighbor]) {
                // Create a new visited bitmask for the next state
                vector<bool> new_visited = visited;
                new_visited[neighbor] = true;
                // Push the new state onto the stack
                stk.emplace(neighbor, new_visited, steps + 1);
            }
        }
    }

    // Output the result
    cout << "The longest hike contains " << max_steps << " steps." << endl;
    return 0;
}

