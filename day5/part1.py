import argparse
from collections import defaultdict
import bisect

def parse_input(input_file):
    with open(input_file, 'r') as file:
        lines = file.readlines()

    # Extract seed numbers
    seeds = list(map(int, lines[0].split(":")[1].strip().split()))
    maps = defaultdict(list)

    # Read and parse all the maps
    current_map_name = None
    for line in lines[2:]:
        line = line.strip()
        if not line:
            continue  # Skip empty lines
        if ':' in line:
            current_map_name = line.strip().split(':')[0]
        else:
            parts = line.split()
            if len(parts) != 3:
                continue  # Skip lines that don't have exactly 3 numbers
            destination_start, source_start, length = map(int, parts)
            maps[current_map_name].append((source_start, destination_start, length))
    
    return seeds, maps

def build_intervals(map_data):
    # Build sorted intervals for efficient lookups
    intervals = []
    for source_start, destination_start, length in map_data:
        intervals.append((source_start, source_start + length - 1, destination_start))
    
    # Sort intervals by source start
    intervals.sort()
    return intervals

def lookup_mapping(intervals, number):
    # Use binary search to find the correct interval
    index = bisect.bisect_right(intervals, (number,)) - 1
    
    # Check if the index is within bounds and if the number falls within the range of that interval
    if index >= 0 and intervals[index][0] <= number <= intervals[index][1]:
        # Calculate the mapped destination number
        destination_start = intervals[index][2]
        source_start = intervals[index][0]
        return destination_start + (number - source_start)
    else:
        # If the number is not in any interval, it maps to itself
        return number

def find_lowest_location(seeds, maps):
    # Build interval lists for all mappings
    seed_to_soil = build_intervals(maps['seed-to-soil map'])
    soil_to_fertilizer = build_intervals(maps['soil-to-fertilizer map'])
    fertilizer_to_water = build_intervals(maps['fertilizer-to-water map'])
    water_to_light = build_intervals(maps['water-to-light map'])
    light_to_temperature = build_intervals(maps['light-to-temperature map'])
    temperature_to_humidity = build_intervals(maps['temperature-to-humidity map'])
    humidity_to_location = build_intervals(maps['humidity-to-location map'])

    # Apply mappings and find the lowest location
    lowest_location = float('inf')

    for seed in seeds:
        soil = lookup_mapping(seed_to_soil, seed)
        fertilizer = lookup_mapping(soil_to_fertilizer, soil)
        water = lookup_mapping(fertilizer_to_water, fertilizer)
        light = lookup_mapping(water_to_light, water)
        temperature = lookup_mapping(light_to_temperature, light)
        humidity = lookup_mapping(temperature_to_humidity, temperature)
        location = lookup_mapping(humidity_to_location, humidity)
        lowest_location = min(lowest_location, location)

    return lowest_location

def main():
    # Set up argument parser
    parser = argparse.ArgumentParser(description="Find the lowest location number corresponding to any of the initial seed numbers.")
    parser.add_argument('input_file', type=str, help='The input file containing almanac data')
    
    # Parse arguments
    args = parser.parse_args()

    # Parse input data
    seeds, maps = parse_input(args.input_file)

    # Find and print the lowest location
    lowest_location = find_lowest_location(seeds, maps)
    print(lowest_location)

if __name__ == "__main__":
    main()

