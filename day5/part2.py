import argparse
from collections import defaultdict
import bisect

def parse_input(input_file):
    with open(input_file, 'r') as file:
        lines = file.readlines()

    # Extract seed ranges
    seed_ranges = list(map(int, lines[0].split(":")[1].strip().split()))
    seed_intervals = [(seed_ranges[i], seed_ranges[i + 1]) for i in range(0, len(seed_ranges), 2)]
    
    maps = defaultdict(list)

    # Read and parse all the maps
    current_map_name = None
    for line in lines[2:]:
        line = line.strip()
        if not line:
            continue  # Skip empty lines
        if ':' in line:
            current_map_name = line.strip().split(':')[0]
        else:
            parts = line.split()
            if len(parts) != 3:
                continue  # Skip lines that don't have exactly 3 numbers
            destination_start, source_start, length = map(int, parts)
            maps[current_map_name].append((source_start, destination_start, length))
    
    return seed_intervals, maps

def build_intervals(map_data):
    # Build sorted intervals for efficient lookups
    intervals = []
    for source_start, destination_start, length in map_data:
        intervals.append((source_start, source_start + length - 1, destination_start))
    
    # Sort intervals by source start
    intervals.sort()
    return intervals

def lookup_range(intervals, start, end):
    # Map a range of numbers using the provided intervals
    mapped_ranges = []

    # Find the starting index using binary search
    i = bisect.bisect_right(intervals, (start,)) - 1

    while start <= end:
        if i < len(intervals) and i >= 0:
            source_start, source_end, destination_start = intervals[i]

            if source_end < start:
                i += 1
                continue

            if source_start > end:
                break

            if source_start <= start <= source_end:
                # Map range [start, min(end, source_end)] to destination range
                range_length = min(end, source_end) - start + 1
                mapped_start = destination_start + (start - source_start)
                mapped_end = mapped_start + range_length - 1
                mapped_ranges.append((mapped_start, mapped_end))
                start = source_end + 1
            else:
                # If the current interval does not contain the start, map directly
                mapped_ranges.append((start, min(end, source_start - 1)))
                start = source_start
        else:
            # If there are no more relevant intervals, map directly
            mapped_ranges.append((start, end))
            break

    return mapped_ranges

def find_lowest_location(seed_intervals, maps):
    # Build interval lists for all mappings
    seed_to_soil = build_intervals(maps['seed-to-soil map'])
    soil_to_fertilizer = build_intervals(maps['soil-to-fertilizer map'])
    fertilizer_to_water = build_intervals(maps['fertilizer-to-water map'])
    water_to_light = build_intervals(maps['water-to-light map'])
    light_to_temperature = build_intervals(maps['light-to-temperature map'])
    temperature_to_humidity = build_intervals(maps['temperature-to-humidity map'])
    humidity_to_location = build_intervals(maps['humidity-to-location map'])

    # Process each seed range
    current_ranges = seed_intervals
    
    for intervals in [seed_to_soil, soil_to_fertilizer, fertilizer_to_water, 
                      water_to_light, light_to_temperature, 
                      temperature_to_humidity, humidity_to_location]:
        new_ranges = []
        for (start, length) in current_ranges:
            end = start + length - 1
            mapped = lookup_range(intervals, start, end)
            new_ranges.extend(mapped)
        current_ranges = new_ranges

    # Find the minimum location from the final ranges
    lowest_location = min(start for (start, end) in current_ranges if start > 0)  # Ensure the start is not zero
    return lowest_location

def main():
    # Set up argument parser
    parser = argparse.ArgumentParser(description="Find the lowest location number corresponding to any of the initial seed numbers.")
    parser.add_argument('input_file', type=str, help='The input file containing almanac data')
    
    # Parse arguments
    args = parser.parse_args()

    # Parse input data
    seed_intervals, maps = parse_input(args.input_file)

    # Find and print the lowest location
    lowest_location = find_lowest_location(seed_intervals, maps)
    print(lowest_location)

if __name__ == "__main__":
    main()

